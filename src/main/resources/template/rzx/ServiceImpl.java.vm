package ${package};

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
#if(${isContainSort})
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
#{end}

import org.apache.commons.collections.CollectionUtils;

import org.apache.commons.lang3.StringUtils;
import java.util.stream.Collectors;
import java.util.Comparator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import ${tb}.OrderModel;
import ${SQLUtilPackage}.QueryResult;
import ${MapperPackage}.${className}Mapper;
import ${DOPackage}.${className}DO;
import ${ProviderPackage}.${className}DAOProvider;
import ${ServicePackage}.${className}Service;
import ${ConverterPackage}.${className}Converter;
import ${DTOPackage}.${className}DTO;

@Service
public class ${className}ServiceImpl implements ${className}Service {
	#if(${isContainSort})

	private Lock insertLock = new ReentrantLock();
	#{end}
	
	@Autowired
	private ${className}Converter ${classname}Converter;
	@Autowired
	private ${className}Mapper ${classname}Mapper;
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	@Override
	public Boolean insert${className}(${className}DTO ${classname}DTO) throws Exception {
		${className}DO ${classname}DO = ${classname}Converter.convert(${classname}DTO);
		${classname}Mapper.insert${className}(${classname}DO);
		return Boolean.TRUE;
	}
	#if(${isContainSort})
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	@Override
	public Boolean insert${className}AndUpdateSort(#parse("template/rzx/ComponentBasicCondition.java.vm") ${className}DTO ${classname}DTO) throws Exception {
		try {
			insertLock.lock();
			// 1. 设置sort
			${classname}DTO.setSort(1 + this.getMaxSort(#parse("template/rzx/ComponentBasicConditionValue2.java.vm")));
			// 2. 保存新节点
			this.insert${className}(${classname}DTO);
			// 3. 更新节点排序
			this.updateSort(#parse("template/rzx/ComponentBasicConditionValue2.java.vm"));
        }finally {
        	insertLock.unlock();
        }
		return Boolean.TRUE;
	}
	#{end}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	@Override
	public Boolean batchInsert${className}(List<${className}DTO> ${classname}DTOList) throws Exception {
		if(CollectionUtils.isEmpty(${classname}DTOList)) { return Boolean.TRUE; }
		List<${className}DO> ${classname}DOList = ${classname}Converter.convertList(${classname}DTOList);
		${classname}Mapper.batchInsert${className}(${classname}DOList);
		return Boolean.TRUE;
	}	
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	@Override
	public Boolean batchInsert${className}4Less(List<${className}DTO> ${classname}DTOList) throws Exception {
	    if(CollectionUtils.isEmpty(${classname}DTOList)) { return Boolean.TRUE; }
		for(${className}DTO ${classname}DTO : ${classname}DTOList) {
			this.insert${className}(${classname}DTO);
		}
		return Boolean.TRUE;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	@Override
	public Boolean update${className}(${className}DTO ${classname}DTO) throws Exception {
		${className}DO ${classname}DO = ${classname}Converter.convert(${classname}DTO);
		${classname}Mapper.update${className}(${classname}DO);
		return Boolean.TRUE;
	}
	
	@Transactional(propagation = Propagation.REQUIRED,rollbackFor = Exception.class)
	@Override
	public Boolean batchUpdate${className}(List<${className}DTO> ${classname}DTOList) throws Exception {
		if(CollectionUtils.isEmpty(${classname}DTOList)) { return Boolean.TRUE; }
		for(${className}DTO ${classname}DTO : ${classname}DTOList) {
			this.update${className}(${classname}DTO);
		}
		return Boolean.TRUE;
	}
	#if(${isContainSort})

	@Transactional(propagation = Propagation.REQUIRED,rollbackFor = Exception.class)
	@Override
	public Boolean update${className}Sort(String id, Integer offset) throws Exception {
		/** 参数验证 **/
		if (StringUtils.isBlank(id) || offset == 0) { return Boolean.TRUE; }
		
        /** 查询同级节点 **/
		${className}DTO ${classname}DTO = this.get${className}ById(id);
#foreach (${cm} in ${determinerList4List})
#if($determinerList4BasicProps.contains(${cm.columnName}))
		${cm.attrType} ${cm.attrname} = ${classname}DTO.get${cm.attrName2}();
#{end}#{end}
        Integer sort = ${classname}DTO.getSort();
        
        List<${className}DTO> ${classname}DTOList = this.list${className}(#parse("template/rzx/test.java.vm"));
        
        Integer maxSort = ${classname}DTOList.stream().max((o1, o2) -> o1.getSort() - o2.getSort()).get().getSort();
        if (offset < 0) {
        	${classname}DTOList = ${classname}DTOList.stream()
                    .filter(dti -> (dti.getSort() >= sort + offset) && (dti.getSort() < sort)).collect(Collectors.toList());
        	${classname}DTOList.forEach(dti -> dti.setSort(dti.getSort() + 1));
        } 
        if (offset > 0) {
        	${classname}DTOList = ${classname}DTOList.stream()
                    .filter(dti -> (dti.getSort() <= sort + offset) && (dti.getSort() > sort)).collect(Collectors.toList());
        	${classname}DTOList.forEach(dti -> dti.setSort(dti.getSort() - 1));
        }
        //处理排序字段小于零或大于最大值情况
        Integer newSort = sort + offset;
        if (newSort <= 0) {
            newSort = 1;
        } else if (newSort > maxSort) {
            newSort = maxSort;
        }
        ${classname}DTO.setSort(newSort);
        ${classname}DTOList.add(${classname}DTO);
        
        this.batchUpdate${className}(${classname}DTOList);
        
        return Boolean.TRUE;
	}
	#{end}
		
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	@Override
	public Boolean delete${className}(String id) throws Exception {
		return this.batchDelete${className}(#parse("template/rzx/ComponentBasicConditionValue.java.vm") Arrays.asList(id));
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	@Override
	public Boolean batchDelete${className}(List<String> ids) throws Exception {
		${classname}Mapper.batchDelete${className}(ids);
		return Boolean.TRUE;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	@Override
	public Boolean batchDelete${className}(#parse("template/rzx/ComponentBasicCondition.java.vm") List<String> ids) throws Exception {
		this.batchDelete${className}(ids);
		return Boolean.TRUE;
	}

	@Override
	public ${className}DTO get${className}ById(String id) throws Exception {
		${className}DO ${classname}DO = ${classname}Mapper.get${className}ById(id);
		${className}DTO ${classname}DTO = ${classname}Converter.invert(${classname}DO);
		return ${classname}DTO;
	}
	
	@Override
	public List<${className}DTO> list${className}ByIds(List<String> ids) throws Exception {
		List<${className}DO> ${classname}DOList = ${classname}Mapper.list${className}ByIds(ids);
		List<${className}DTO> ${classname}DTOList = ${classname}Converter.invertList(${classname}DOList);
		return ${classname}DTOList;
	}

	@Override
	public List<${className}DTO> list${className}(#parse("template/rzx/ComponentListCondition.java.vm")) throws Exception {
		List<${className}DO> ${classname}DOList = ${classname}Mapper.list${className}(#parse("template/rzx/ComponentListConditionValue.java.vm"));
		List<${className}DTO> ${classname}DTOList = ${classname}Converter.invertList(${classname}DOList);
		return ${classname}DTOList;
	}

	@Override
	public QueryResult<${className}DTO> page${className}(#parse("template/rzx/ComponentPageCondition.java.vm")) throws Exception {
		/**设置分页**/
		String order = ${className}DAOProvider.createOrder(orderList);
		PageHelper.startPage(pageIndex.intValue(), pageSize, order);
		
		List<${className}DO> ${classname}DOList = ${classname}Mapper.list${className}(#parse("template/rzx/ComponentListConditionValue.java.vm"));
		PageInfo<${className}DO> page = new PageInfo<>(${classname}DOList);
		Long total = page.getTotal();
		List<${className}DTO> ${classname}DTOList = ${classname}Converter.invertList(${classname}DOList);
		
		QueryResult<${className}DTO> result = new QueryResult<>();
		result.setResult(${classname}DTOList);
		result.setTotalCount(total);
		
		return result;
	}
	#if(${isContainSort})
	
	// 获取最大的sort
	private Integer getMaxSort(#parse("template/rzx/ComponentBasicCondition2.java.vm")) throws Exception {
		Integer maxSort = 0;
		List<${className}DTO> acList = this.list${className}(#parse("template/rzx/test.java.vm"));
		if(CollectionUtils.isNotEmpty(acList)) {
			maxSort = acList.stream().mapToInt(obj -> obj.getSort()).max().getAsInt();
		}
		return maxSort;
	}
	// 更新sort（整理为连续节点，如：1，3，4，7 -》1，2，3，4）
	@Transactional(propagation = Propagation.REQUIRED,rollbackFor = Exception.class)
	private synchronized void updateSort(#parse("template/rzx/ComponentBasicCondition2.java.vm")) throws Exception {
		List<${className}DTO> acList = this.list${className}(#parse("template/rzx/test.java.vm"));
		if(CollectionUtils.isNotEmpty(acList)) {
			acList = acList.stream()
					.sorted(Comparator.comparing(${className}DTO::getSort).thenComparing(${className}DTO::getCreateAt, Comparator.reverseOrder()))
				.collect(Collectors.toList());
			
			Integer idx = 0;
			List<${className}DTO> updateSortList = new ArrayList<>();
			for(${className}DTO ac : acList) {
				if(ac.getSort() == ++idx) { continue; }
				ac.setSort(idx);
				updateSortList.add(ac);
			}
			
			this.batchUpdate${className}(updateSortList);
		}
	}
	#{end}
}