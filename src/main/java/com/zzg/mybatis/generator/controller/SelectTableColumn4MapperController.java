package com.zzg.mybatis.generator.controller;

import com.zzg.mybatis.generator.model.ColumnEntity;
import com.zzg.mybatis.generator.model.UITableColumnVO;
import com.zzg.mybatis.generator.util.GenUtils;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Owen on 6/20/16.
 */
public class SelectTableColumn4MapperController extends BaseFXController {

	@FXML
    private TableView<UITableColumnVO> columnListView4Mapper;
    @FXML
    private TableColumn<UITableColumnVO, Boolean> checkedColumn;
    @FXML
    private TableColumn<UITableColumnVO, String> columnNameColumn;
    @FXML
    private TableColumn<UITableColumnVO, String> jdbcTypeColumn;
    @FXML
    private TableColumn<UITableColumnVO, String> javaTypeColumn;
    @FXML
    private TableColumn<UITableColumnVO, String> propertyNameColumn;
    @FXML
    private TableColumn<UITableColumnVO, String> comments;
    @FXML
    private TableColumn<UITableColumnVO, String> typeHandlerColumn;

    private MainUIController mainUIController;

    private String tableName;

    @FXML
    private TableColumn<UITableColumnVO, Boolean> checkedEqual;
    @FXML
    private TableColumn<UITableColumnVO, Boolean> checkedIn;
    @FXML
    private TableColumn<UITableColumnVO, Boolean> checkedLike;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // cellvaluefactory
        //checkedColumn.setCellValueFactory(new PropertyValueFactory<>("checked"));
        
        checkedEqual.setCellValueFactory(new PropertyValueFactory<>("checkedEqual"));
        checkedIn.setCellValueFactory(new PropertyValueFactory<>("checkedIn"));
        checkedLike.setCellValueFactory(new PropertyValueFactory<>("checkedLike"));
        
        
        columnNameColumn.setCellValueFactory(new PropertyValueFactory<>("columnName"));
        jdbcTypeColumn.setCellValueFactory(new PropertyValueFactory<>("jdbcType"));
        propertyNameColumn.setCellValueFactory(new PropertyValueFactory<>("propertyName"));
        comments.setCellValueFactory(new PropertyValueFactory<>("comments"));
        //typeHandlerColumn.setCellValueFactory(new PropertyValueFactory<>("typeHandler"));
        // Cell Factory that customize how the cell should render
        //checkedColumn.setCellFactory(CheckBoxTableCell.forTableColumn(checkedColumn));
        
        checkedEqual.setCellFactory(CheckBoxTableCell.forTableColumn(checkedEqual));
        checkedIn.setCellFactory(CheckBoxTableCell.forTableColumn(checkedIn));
        checkedLike.setCellFactory(CheckBoxTableCell.forTableColumn(checkedLike));
        
        javaTypeColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        // handle commit event to save the user input data
        javaTypeColumn.setOnEditCommit(event -> {
            event.getTableView().getItems().get(event.getTablePosition().getRow()).setJavaType(event.getNewValue());
        });
        propertyNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        propertyNameColumn.setOnEditCommit(event -> {
            event.getTableView().getItems().get(event.getTablePosition().getRow()).setPropertyName(event.getNewValue());
        });
        comments.setCellFactory(TextFieldTableCell.forTableColumn());
        comments.setOnEditCommit(event -> {
        	event.getTableView().getItems().get(event.getTablePosition().getRow()).setPropertyName(event.getNewValue());
        });
        //typeHandlerColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        /*typeHandlerColumn.setOnEditCommit(event -> {
            event.getTableView().getItems().get(event.getTablePosition().getRow()).setTypeHandle(event.getNewValue());
        });*/
    }

    @FXML
    public void ok() {
        ObservableList<UITableColumnVO> items = columnListView4Mapper.getItems();
        if (items != null && items.size() > 0) {
            List<ColumnEntity> ceEqualList = new ArrayList<>();
            List<ColumnEntity> ceInList = new ArrayList<>();
            List<ColumnEntity> ceLikeList = new ArrayList<>();
            items.stream().forEach(item -> {
//            	列名转换成Java属性名
                String attrName = GenUtils.columnToJava(item.getColumnName(), Boolean.TRUE);
                String attrName2 = GenUtils.columnToJava(item.getColumnName(), Boolean.FALSE);
                //	列的数据类型，转换成Java类型
                String attrType = GenUtils.getConfig().getString(item.getJdbcType(), "unknowType");
            	ColumnEntity columnEntity = new ColumnEntity(item.getColumnName(), item.getJdbcType(), item.getComments(), attrName, StringUtils.uncapitalize(attrName),
            			attrType, null);
            	columnEntity.setAttrName2(attrName2);
                if(item.getCheckedEqual()) {
                	columnEntity.setType(1);
                	ceEqualList.add(columnEntity);
                } else if(item.getCheckedIn()) {
                	columnEntity.setType(2);
                	ceInList.add(columnEntity);
                } else if(item.getCheckedLike()) {
                	columnEntity.setType(3);
                	ceLikeList.add(columnEntity);
                }
            });
            mainUIController.setCeEqualList(ceEqualList);
            mainUIController.setCeInList(ceInList);
            mainUIController.setCeLikeList(ceLikeList);
            
            mainUIController.setCeAllList(new ArrayList<>());
            if(CollectionUtils.isNotEmpty(ceEqualList)) { mainUIController.getCeAllList().addAll(ceEqualList); }
            if(CollectionUtils.isNotEmpty(ceInList)) { mainUIController.getCeAllList().addAll(ceInList); }
            if(CollectionUtils.isNotEmpty(ceLikeList)) {
            	mainUIController.getCeAllList().add(new ColumnEntity("keyword", "VARCHAR", "comments", "keyword", "keyword", "String", null, 3, "Keyword"));
            }
            
        }
        getDialogStage().close();
    }

    @FXML
    public void cancel() {
        getDialogStage().close();
    }

    public void setColumnList(ObservableList<UITableColumnVO> columns) {
    	columnListView4Mapper.setItems(columns);
    }

    public void setMainUIController(MainUIController mainUIController) {
        this.mainUIController = mainUIController;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }


}
