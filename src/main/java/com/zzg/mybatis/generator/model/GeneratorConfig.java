package com.zzg.mybatis.generator.model;

/**
 *
 * GeneratorConfig is the Config of mybatis generator config exclude database
 * config
 *
 * Created by Owen on 6/16/16.
 */
public class GeneratorConfig {

	/**
	 * 本配置的名称
	 */
	private String name;

	private String connectorJarPath;

	private String projectFolder;

	private String modelPackage;

	private String modelPackageTargetFolder;

	private String daoPackage;

	private String daoTargetFolder;

	private String mapperName;

	private String mappingXMLPackage;

	private String mappingXMLTargetFolder;

	private String tableName;

	private String domainObjectName;

	private boolean offsetLimit;

	private boolean comment;

	private boolean overrideXML;

	private boolean needToStringHashcodeEquals;

	private boolean needForUpdate;

	private boolean annotationDAO;

	private boolean annotation;

	private boolean useActualColumnNames;

	private boolean useExample;

	private String generateKeys;

	private String encoding;

	private boolean useTableNameAlias;

	private boolean useDAOExtendStyle;

    private boolean useSchemaPrefix;

    private boolean jsr310Support;

    
    /** 版本v1.0.0 开始 **/
    private String dom;
    private String provider;
    private String mapper;
    private String dto;
    private String converter;
    private String service;
    private String serviceImpl;
    private String requestSave;
    private String requestUpdate;
    private String requestSort;
    private String response;
    private String exception;
    private String controller;
    private String domPackage;
    private String providerPackage;
    private String mapperPackage;
    private String dtoPackage;
    private String converterPackage;
    private String servicePackage;
    private String serviceImplPackage;
    private String requestSavePackage;
    private String requestUpdatePackage;
    private String requestSortPackage;
    private String responsePackage;
    private String exceptionPackage;
    private String controllerPackage;
    private String domTargetProject;
    private String providerTargetProject;
    private String mapperTargetProject;
    private String dtoTargetProject;
    private String converterTargetProject;
    private String serviceTargetProject;
    private String serviceImplTargetProject;
    private String requestSaveTargetProject;
    private String requestUpdateTargetProject;
    private String requestSortTargetProject;
    private String responseTargetProject;
    private String exceptionTargetProject;
    private String controllerTargetProject;
    
    private String tb; 
    private String cv;
    private String ep;
    private String ct;
    private String rp;
    private String apisort;
    private boolean isremoveprefix;
    private String SQLUtilPackage;
    /** 版本v1.0.0 结束 **/    
    
    public boolean isJsr310Support() {
        return jsr310Support;
    }

    public void setJsr310Support(boolean jsr310Support) {
        this.jsr310Support = jsr310Support;
    }

    public boolean isUseSchemaPrefix() {
        return useSchemaPrefix;
    }

    public void setUseSchemaPrefix(boolean useSchemaPrefix) {
        this.useSchemaPrefix = useSchemaPrefix;
    }

	public boolean isUseExample() {
		return useExample;
	}

	public void setUseExample(boolean useExample) {
		this.useExample = useExample;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getDomainObjectName() {
		return domainObjectName;
	}

	public void setDomainObjectName(String domainObjectName) {
		this.domainObjectName = domainObjectName;
	}

	public String getConnectorJarPath() {
		return connectorJarPath;
	}

	public void setConnectorJarPath(String connectorJarPath) {
		this.connectorJarPath = connectorJarPath;
	}

	public String getProjectFolder() {
		return projectFolder;
	}

	public void setProjectFolder(String projectFolder) {
		this.projectFolder = projectFolder;
	}

	public String getModelPackage() {
		return modelPackage;
	}

	public void setModelPackage(String modelPackage) {
		this.modelPackage = modelPackage;
	}

	public String getModelPackageTargetFolder() {
		return modelPackageTargetFolder;
	}

	public void setModelPackageTargetFolder(String modelPackageTargetFolder) {
		this.modelPackageTargetFolder = modelPackageTargetFolder;
	}

	public String getDaoPackage() {
		return daoPackage;
	}

	public void setDaoPackage(String daoPackage) {
		this.daoPackage = daoPackage;
	}

	public String getDaoTargetFolder() {
		return daoTargetFolder;
	}

	public void setDaoTargetFolder(String daoTargetFolder) {
		this.daoTargetFolder = daoTargetFolder;
	}

	public String getMappingXMLPackage() {
		return mappingXMLPackage;
	}

	public void setMappingXMLPackage(String mappingXMLPackage) {
		this.mappingXMLPackage = mappingXMLPackage;
	}

	public String getMappingXMLTargetFolder() {
		return mappingXMLTargetFolder;
	}

	public void setMappingXMLTargetFolder(String mappingXMLTargetFolder) {
		this.mappingXMLTargetFolder = mappingXMLTargetFolder;
	}

	public boolean isOffsetLimit() {
		return offsetLimit;
	}

	public void setOffsetLimit(boolean offsetLimit) {
		this.offsetLimit = offsetLimit;
	}

	public boolean isComment() {
		return comment;
	}

	public void setComment(boolean comment) {
		this.comment = comment;
	}

    public boolean isNeedToStringHashcodeEquals() {
        return needToStringHashcodeEquals;
    }

    public void setNeedToStringHashcodeEquals(boolean needToStringHashcodeEquals) {
        this.needToStringHashcodeEquals = needToStringHashcodeEquals;
    }

	public boolean isNeedForUpdate() {
		return needForUpdate;
	}

	public void setNeedForUpdate(boolean needForUpdate) {
		this.needForUpdate = needForUpdate;
	}

	public boolean isAnnotationDAO() {
		return annotationDAO;
	}

	public void setAnnotationDAO(boolean annotationDAO) {
		this.annotationDAO = annotationDAO;
	}

	public boolean isAnnotation() {
		return annotation;
	}

	public void setAnnotation(boolean annotation) {
		this.annotation = annotation;
	}

	public boolean isUseActualColumnNames() {
		return useActualColumnNames;
	}

	public void setUseActualColumnNames(boolean useActualColumnNames) {
		this.useActualColumnNames = useActualColumnNames;
	}

	public String getMapperName() {
		return mapperName;
	}

	public void setMapperName(String mapperName) {
		this.mapperName = mapperName;
	}

	public String getGenerateKeys() {
		return generateKeys;
	}

	public void setGenerateKeys(String generateKeys) {
		this.generateKeys = generateKeys;
	}

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

	public boolean getUseTableNameAlias() {
		return useTableNameAlias;
	}

	public void setUseTableNameAlias(boolean useTableNameAlias) {
		this.useTableNameAlias = useTableNameAlias;
	}

	public boolean isUseTableNameAlias() {
		return useTableNameAlias;
	}

	public boolean isOverrideXML() {
		return overrideXML;
	}

	public void setOverrideXML(boolean overrideXML) {
		this.overrideXML = overrideXML;
	}

	public void setUseDAOExtendStyle(boolean useDAOExtendStyle) {
		this.useDAOExtendStyle = useDAOExtendStyle;
	}

	public boolean isUseDAOExtendStyle() {
		return useDAOExtendStyle;
	}

	public String getDom() {
		return dom;
	}

	public String getProvider() {
		return provider;
	}

	public String getMapper() {
		return mapper;
	}

	public String getDto() {
		return dto;
	}

	public String getConverter() {
		return converter;
	}

	public String getService() {
		return service;
	}

	public String getServiceImpl() {
		return serviceImpl;
	}

	public String getRequestSave() {
		return requestSave;
	}

	public String getRequestUpdate() {
		return requestUpdate;
	}

	public String getRequestSort() {
		return requestSort;
	}

	public String getResponse() {
		return response;
	}

	public String getException() {
		return exception;
	}

	public String getController() {
		return controller;
	}

	public String getDomPackage() {
		return domPackage;
	}

	public String getProviderPackage() {
		return providerPackage;
	}

	public String getMapperPackage() {
		return mapperPackage;
	}

	public String getDtoPackage() {
		return dtoPackage;
	}

	public String getConverterPackage() {
		return converterPackage;
	}

	public String getServicePackage() {
		return servicePackage;
	}

	public String getServiceImplPackage() {
		return serviceImplPackage;
	}

	public String getRequestSavePackage() {
		return requestSavePackage;
	}

	public String getRequestUpdatePackage() {
		return requestUpdatePackage;
	}

	public String getRequestSortPackage() {
		return requestSortPackage;
	}

	public String getResponsePackage() {
		return responsePackage;
	}

	public String getExceptionPackage() {
		return exceptionPackage;
	}

	public String getControllerPackage() {
		return controllerPackage;
	}

	public String getDomTargetProject() {
		return domTargetProject;
	}

	public String getProviderTargetProject() {
		return providerTargetProject;
	}

	public String getMapperTargetProject() {
		return mapperTargetProject;
	}

	public String getDtoTargetProject() {
		return dtoTargetProject;
	}

	public String getConverterTargetProject() {
		return converterTargetProject;
	}

	public String getServiceTargetProject() {
		return serviceTargetProject;
	}

	public String getServiceImplTargetProject() {
		return serviceImplTargetProject;
	}

	public String getRequestSaveTargetProject() {
		return requestSaveTargetProject;
	}

	public String getRequestUpdateTargetProject() {
		return requestUpdateTargetProject;
	}

	public String getRequestSortTargetProject() {
		return requestSortTargetProject;
	}

	public String getResponseTargetProject() {
		return responseTargetProject;
	}

	public String getExceptionTargetProject() {
		return exceptionTargetProject;
	}

	public String getControllerTargetProject() {
		return controllerTargetProject;
	}

	public void setDom(String dom) {
		this.dom = dom;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public void setMapper(String mapper) {
		this.mapper = mapper;
	}

	public void setDto(String dto) {
		this.dto = dto;
	}

	public void setConverter(String converter) {
		this.converter = converter;
	}

	public void setService(String service) {
		this.service = service;
	}

	public void setServiceImpl(String serviceImpl) {
		this.serviceImpl = serviceImpl;
	}

	public void setRequestSave(String requestSave) {
		this.requestSave = requestSave;
	}

	public void setRequestUpdate(String requestUpdate) {
		this.requestUpdate = requestUpdate;
	}

	public void setRequestSort(String requestSort) {
		this.requestSort = requestSort;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public void setController(String controller) {
		this.controller = controller;
	}

	public void setDomPackage(String domPackage) {
		this.domPackage = domPackage;
	}

	public void setProviderPackage(String providerPackage) {
		this.providerPackage = providerPackage;
	}

	public void setMapperPackage(String mapperPackage) {
		this.mapperPackage = mapperPackage;
	}

	public void setDtoPackage(String dtoPackage) {
		this.dtoPackage = dtoPackage;
	}

	public void setConverterPackage(String converterPackage) {
		this.converterPackage = converterPackage;
	}

	public void setServicePackage(String servicePackage) {
		this.servicePackage = servicePackage;
	}

	public void setServiceImplPackage(String serviceImplPackage) {
		this.serviceImplPackage = serviceImplPackage;
	}

	public void setRequestSavePackage(String requestSavePackage) {
		this.requestSavePackage = requestSavePackage;
	}

	public void setRequestUpdatePackage(String requestUpdatePackage) {
		this.requestUpdatePackage = requestUpdatePackage;
	}

	public void setRequestSortPackage(String requestSortPackage) {
		this.requestSortPackage = requestSortPackage;
	}

	public void setResponsePackage(String responsePackage) {
		this.responsePackage = responsePackage;
	}

	public void setExceptionPackage(String exceptionPackage) {
		this.exceptionPackage = exceptionPackage;
	}

	public void setControllerPackage(String controllerPackage) {
		this.controllerPackage = controllerPackage;
	}

	public void setDomTargetProject(String domTargetProject) {
		this.domTargetProject = domTargetProject;
	}

	public void setProviderTargetProject(String providerTargetProject) {
		this.providerTargetProject = providerTargetProject;
	}

	public void setMapperTargetProject(String mapperTargetProject) {
		this.mapperTargetProject = mapperTargetProject;
	}

	public void setDtoTargetProject(String dtoTargetProject) {
		this.dtoTargetProject = dtoTargetProject;
	}

	public void setConverterTargetProject(String converterTargetProject) {
		this.converterTargetProject = converterTargetProject;
	}

	public void setServiceTargetProject(String serviceTargetProject) {
		this.serviceTargetProject = serviceTargetProject;
	}

	public void setServiceImplTargetProject(String serviceImplTargetProject) {
		this.serviceImplTargetProject = serviceImplTargetProject;
	}

	public void setRequestSaveTargetProject(String requestSaveTargetProject) {
		this.requestSaveTargetProject = requestSaveTargetProject;
	}

	public void setRequestUpdateTargetProject(String requestUpdateTargetProject) {
		this.requestUpdateTargetProject = requestUpdateTargetProject;
	}

	public void setRequestSortTargetProject(String requestSortTargetProject) {
		this.requestSortTargetProject = requestSortTargetProject;
	}

	public void setResponseTargetProject(String responseTargetProject) {
		this.responseTargetProject = responseTargetProject;
	}

	public void setExceptionTargetProject(String exceptionTargetProject) {
		this.exceptionTargetProject = exceptionTargetProject;
	}

	public void setControllerTargetProject(String controllerTargetProject) {
		this.controllerTargetProject = controllerTargetProject;
	}

	public String getTb() {
		return tb;
	}

	public void setTb(String tb) {
		this.tb = tb;
	}

	public String getSQLUtilPackage() {
		return SQLUtilPackage;
	}

	public void setSQLUtilPackage(String sQLUtilPackage) {
		SQLUtilPackage = sQLUtilPackage;
	}

	public String getCv() {
		return cv;
	}

	public void setCv(String cv) {
		this.cv = cv;
	}

	public String getEp() {
		return ep;
	}

	public void setEp(String ep) {
		this.ep = ep;
	}

	public String getCt() {
		return ct;
	}

	public void setCt(String ct) {
		this.ct = ct;
	}

	public String getRp() {
		return rp;
	}

	public void setRp(String rp) {
		this.rp = rp;
	}

	public String getApisort() {
		return apisort;
	}

	public void setApisort(String apisort) {
		this.apisort = apisort;
	}

	public boolean isIsremoveprefix() {
		return isremoveprefix;
	}

	public void setIsremoveprefix(boolean isremoveprefix) {
		this.isremoveprefix = isremoveprefix;
	}
}
