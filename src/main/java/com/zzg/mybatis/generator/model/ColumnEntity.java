package com.zzg.mybatis.generator.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;

/**
 * 列的属性
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2016年12月20日 上午12:01:45
 */
public class ColumnEntity {
	//列名
    private String columnName;
    //列名类型
    private String dataType;
    //列名备注
    private String comments;
    
    //属性名称(第一个字母大写)，如：user_name => UserName
    private String attrName;
    //属性名称(第一个字母小写)，如：user_name => userName
    private String attrname;
    //属性类型
    private String attrType;
    //auto_increment
    private String extra;
    
    // 类型（1：=，2：in，3：like，4：业务自定义）
    private Integer type;
    
    //type = 2时：List<String>
    private String attrTypeStr;
  //type = 2时：userNameList
    private String attrnameStr;
    
    //属性名称(第一个字母大写)，如：user_name => UserName(未处理)
    private String attrName2;
    
	public ColumnEntity() {
		super();
	}
	
	public ColumnEntity(String columnName, String dataType, String comments, String attrName, String attrname,
			String attrType, String extra) {
		this.columnName = columnName;
		this.dataType = dataType;
		this.comments = comments;
		this.attrName = attrName;
		this.attrname = attrname;
		this.attrType = attrType;
		this.extra = extra;
	}
	
	public ColumnEntity(String columnName, String dataType, String comments, String attrName, String attrname2,
			String attrType, String extra, Integer type) {
		this.columnName = columnName;
		this.dataType = dataType;
		this.comments = comments;
		this.attrName = attrName;
		this.attrname = attrname2;
		this.attrType = attrType;
		this.extra = extra;
		this.type = type;
	}
	
	public ColumnEntity(String columnName, String dataType, String comments, String attrName, String attrname2,
			String attrType, String extra, Integer type, String attrName2) {
		this.columnName = columnName;
		this.dataType = dataType;
		this.comments = comments;
		this.attrName = attrName;
		this.attrname = attrname2;
		this.attrType = attrType;
		this.extra = extra;
		this.type = type;
		this.attrName2 = attrName2;
	}
	
	public static List<ColumnEntity> getColumnEntity4Page(){
		List<ColumnEntity> columnEntityList = new ArrayList<>();
		columnEntityList.add(new ColumnEntity(null, null, null, null, "pageIndex", "Long", null, 4));
		columnEntityList.add(new ColumnEntity(null, null, null, null, "pageSize",  "Integer", null, 4));
		columnEntityList.add(new ColumnEntity(null, null, null, null, "orderList", "List<OrderModel>", null, 4));
		return columnEntityList;
	}
	
	public static List<ColumnEntity> getColumnEntity4DTO2(){
		List<ColumnEntity> columnEntityList = new ArrayList<>();
		columnEntityList.add(new ColumnEntity(null, null, "创建人员", "CreateBy", "createBy", "String", null, 4));
		columnEntityList.add(new ColumnEntity(null, null, "更新人员", "UpdateBy", "updateBy",  "String", null, 4));
		return columnEntityList;
	}
	
	public static List<ColumnEntity> getColumnEntity4DTO3(){
		List<ColumnEntity> columnEntityList = new ArrayList<>();
		columnEntityList.add(new ColumnEntity(null, null, "更新人员", "UpdateBy", "updateBy",  "String", null, 4));
		return columnEntityList;
	}
	
	public static List<ColumnEntity> getColumns4Save2DTO(){
		List<ColumnEntity> columnEntityList = new ArrayList<>();
		columnEntityList.add(new ColumnEntity(null, null, "创建人员", "CreateBy", "createBy", "String", null, 4));
		columnEntityList.add(new ColumnEntity(null, null, "更新人员", "UpdateBy", "updateBy",  "String", null, 4));
		return columnEntityList;
	}
	
	public static List<ColumnEntity> getColumns4Update2DTO(){
		List<ColumnEntity> columnEntityList = new ArrayList<>();
		columnEntityList.add(new ColumnEntity(null, null, "更新人员", "UpdateBy", "updateBy",  "String", null, 4));
		return columnEntityList;
	}
	
	public static List<ColumnEntity> getColumns4Response2(){
		List<ColumnEntity> columnEntityList = new ArrayList<>();
		columnEntityList.add(new ColumnEntity(null, null, "创建人员", "CreateBy", "createBy", "String", null, 4));
		columnEntityList.add(new ColumnEntity(null, null, "创建时间", "CreateAt", "createAt", "Date",   null, 4));
		columnEntityList.add(new ColumnEntity(null, null, "更新人员", "UpdateBy", "updateBy", "String", null, 4));
		columnEntityList.add(new ColumnEntity(null, null, "更新时间", "UpdateAt", "updateAt", "Date",   null, 4));
		return columnEntityList;
	}
	
	public static List<ColumnEntity> converter2Str(List<ColumnEntity> sourceList){
		if(CollectionUtils.isEmpty(sourceList)) { return Collections.emptyList(); }
		List<ColumnEntity> columnEntityList = new ArrayList<>();
		for(ColumnEntity source : sourceList) {
			source.setAttrTypeStr(2 == source.getType() ? "List<" + source.getAttrType() + ">" : source.getAttrType());
			source.setAttrnameStr(2 == source.getType() ? source.getAttrname() + "List" : source.getAttrname());
			columnEntityList.add(source);
		}
		return columnEntityList;
	}
	
	private static final List<String> FILTER_SAVE_PROPS = new ArrayList<>(Arrays.asList("ID", "EPID"));
	public static List<ColumnEntity> filter4Save(List<ColumnEntity> sourceList){
		if(CollectionUtils.isEmpty(sourceList)) { return Collections.emptyList(); }
		return sourceList.stream().filter(obj -> !FILTER_SAVE_PROPS.contains(obj.getColumnName().toUpperCase())).collect(Collectors.toList());
	}
	
	private static final List<String> FILTER_UPDATE_PROPS = new ArrayList<>(Arrays.asList("EPID", "PROJECT_ID", "SECTION_ID"));
	public static List<ColumnEntity> filter4Update(List<ColumnEntity> sourceList){
		if(CollectionUtils.isEmpty(sourceList)) { return Collections.emptyList(); }
		return sourceList.stream().filter(obj -> !FILTER_UPDATE_PROPS.contains(obj.getColumnName().toUpperCase())).collect(Collectors.toList());
	}
	
	public String getColumnName() {
		return columnName;
	}
	public String getColumnName2Upper() {
		return columnName.toUpperCase();
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getAttrname() {
		return attrname;
	}
	public void setAttrname(String attrname) {
		this.attrname = attrname;
	}
	public String getAttrName() {
		return attrName;
	}
	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}
	public String getAttrType() {
		return attrType;
	}
	public void setAttrType(String attrType) {
		this.attrType = attrType;
	}
	public String getExtra() {
		return extra;
	}
	public void setExtra(String extra) {
		this.extra = extra;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getAttrTypeStr() {
		return attrTypeStr;
	}

	public String getAttrnameStr() {
		return attrnameStr;
	}

	public void setAttrTypeStr(String attrTypeStr) {
		this.attrTypeStr = attrTypeStr;
	}

	public void setAttrnameStr(String attrnameStr) {
		this.attrnameStr = attrnameStr;
	}

	public String getAttrName2() {
		return attrName2;
	}

	public void setAttrName2(String attrName2) {
		this.attrName2 = attrName2;
	}

}
