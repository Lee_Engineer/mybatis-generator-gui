package com.zzg.mybatis.generator.model;

import java.io.Serializable;

public class ItemInfo implements Serializable {
	private static final long serialVersionUID = 4161090738213987998L;

	private String code;
	private String className;
	private String packagePath;
	private String filePath;
	
	public ItemInfo(String code, String className, String packagePath, String filePath) {
		this.code = code;
		this.className = className;
		this.packagePath = packagePath;
		this.filePath = filePath;
	}
	
	public String getCode() {
		return code;
	}
	public String getClassName() {
		return className;
	}
	public String getPackagePath() {
		return packagePath;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public void setPackagePath(String packagePath) {
		this.packagePath = packagePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
}
