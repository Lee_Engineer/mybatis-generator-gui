package com.zzg.mybatis.generator.model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Owen on 6/22/16.
 */
public class UITableColumnVO {

	private BooleanProperty checked = new SimpleBooleanProperty(true); // Default set to true

	private StringProperty columnName = new SimpleStringProperty();

    private StringProperty javaType = new SimpleStringProperty();

    private StringProperty jdbcType = new SimpleStringProperty();

    private StringProperty propertyName = new SimpleStringProperty();

    private StringProperty typeHandle = new SimpleStringProperty();

    private StringProperty comments = new SimpleStringProperty();
    
    private BooleanProperty checkedEqual = new SimpleBooleanProperty(false);
    private BooleanProperty checkedIn = new SimpleBooleanProperty(false);
    private BooleanProperty checkedLike = new SimpleBooleanProperty(false);
    
    public String getColumnName() {
        return columnName.get();
    }

    public void setColumnName(String columnName) {
        this.columnName.set(columnName);
    }

    public String getComments() {
        return comments.get();
    }

    public void setComments(String comments) {
        this.comments.set(comments);
    }
    
    public String getJdbcType() {
        return jdbcType.get();
    }

    public void setJdbcType(String jdbcType) {
        this.jdbcType.set(jdbcType);
    }

    public String getPropertyName() {
        return propertyName.get();
    }

    public void setPropertyName(String propertyName) {
        this.propertyName.set(propertyName);
    }

    public BooleanProperty checkedProperty() {
        return checked;
    }

    public Boolean getChecked() {
        return this.checked.get();
    }

    public void setChecked(Boolean checked) {
        this.checked.set(checked);
    }

    public StringProperty typeHandleProperty() {
        return typeHandle;
    }

    public String getTypeHandle() {
        return typeHandle.get();
    }

    public void setTypeHandle(String typeHandle) {
        this.typeHandle.set(typeHandle);
    }

    public StringProperty columnNameProperty() {
        return columnName;
    }
    
    public StringProperty commentsProperty() {
    	return comments;
    }

    public StringProperty jdbcTypeProperty() {
        return jdbcType;
    }

    public StringProperty propertyNameProperty() {
        return propertyName;
    }

    public String getJavaType() {
        return javaType.get();
    }

    public StringProperty javaTypeProperty() {
        return javaType;
    }

    public void setJavaType(String javaType) {
        this.javaType.set(javaType);
    }
    
    
    public BooleanProperty checkedEqualProperty() {
        return checkedEqual;
    }
    public Boolean getCheckedEqual() {
        return this.checkedEqual.get();
    }
    public void setCheckedEqual(Boolean checkedEqual) {
        this.checkedEqual.set(checkedEqual);
    }
    
    
    public BooleanProperty checkedInProperty() {
        return checkedIn;
    }
    public Boolean getCheckedIn() {
        return this.checkedIn.get();
    }
    public void setCheckedIn(Boolean checkedIn) {
        this.checkedIn.set(checkedIn);
    }
    
    
    public BooleanProperty checkedLikeProperty() {
        return checkedLike;
    }
    public Boolean getCheckedLike() {
        return this.checkedLike.get();
    }
    public void setCheckedLike(Boolean checkedLike) {
        this.checkedLike.set(checkedLike);
    }
}
